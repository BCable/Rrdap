all: Rpkg

clean:
	rm -f Rrdap_*.tar.gz
	rm -rf Rrdap.Rcheck

Rpkg:
	R CMD build .

check:
	R CMD check --as-cran Rrdap_*.tar.gz

devel-check:
	R-devel CMD check --as-cran Rrdap_*.tar.gz
